import {observable, computed, action, decorate, configure, runInAction} from 'mobx';

configure({
    enforceActions: 'observed',
});

export default class EditorStore {
    constructor() {
        this.text = [];
        this.synonyms = [];
        this.style = {};
        this.activeWord = null;
        this.loading = false;
        this.synonymsLoadingError = false;
    }

    get state() {
        return {
            text: this.text,
            activeWord: this.activeWord,
            synonyms: this.synonyms,
            style: this.style,
            loading: this.loading,
            synonymsLoadingError: this.synonymsLoadingError
        };
    }

    setActiveWord(wordId) {
        this.activeWord = wordId;
    }

    setSynonyms(synonym) {
        this.loading = true;
        fetch(`https://api.datamuse.com//words?ml=${synonym}`)
            .then((response) => response.json())
            .then((words) => {
                runInAction(() => {
                    this.loading = false;
                    this.synonyms = words.length ? words.map(({word}) => word) : [];
                    this.synonymsLoadingError = false;
                });
            }).catch(() => {
            runInAction(() => {
                this.loading = false;
                this.synonyms = [];
                this.synonymsLoadingError = true;
            });
        });
    }

    setFontStyle(style) {
        if (this.style[this.activeWord]) {
            this.style[this.activeWord][style] = !this.style[this.activeWord][style];
        } else {
            this.style[this.activeWord] = {
                [style]: true,
            };
        }
    }

    setText() {
        const text = 'A year ago I was in the audience at a gathering of designers in San Francisco. There were four designers on stage, and two of them worked for me. I was there to support them. The topic of design responsibility came up, possibly brought up by one of my designers, I honestly don’t remember the details. What I do remember is that at some point in the discussion I raised my hand and suggested, to this group of designers, that modern design problems were very complex. And we ought to need a license to solve them.';
        this.text = text.trim().split(/\b/g);
    }

    replaceBySynonym(synonym) {
        const ifStartWithCapitalLetter = this.text[this.activeWord][0] === this.text[this.activeWord][0].toUpperCase();
        const capitalizedSynonym = synonym.charAt(0).toUpperCase() + synonym.slice(1);
        this.text[this.activeWord] = ifStartWithCapitalLetter ? capitalizedSynonym : synonym;
    }
};

decorate(EditorStore, {
    text: observable,
    synonyms: observable,
    style: observable,
    state: computed,
    setText: action.bound,
    setActiveWord: action.bound,
    setFontStyle: action.bound,
    setSynonyms: action.bound,
    replaceBySynonym: action.bound,
    loading: observable,
    synonymsLoadingError: observable
});
