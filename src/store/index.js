import {EDITOR_STORE} from '../constants';
import EditorStore from './editor';

const store = {
    [EDITOR_STORE]: new EditorStore(),
};

export default store;
