import React, {Component} from 'react';
import {computed} from 'mobx';
import {inject, observer} from 'mobx-react';
import cx from 'classnames';
import {EDITOR_STORE} from '../../constants';
import './ControlPanel.scoped.css';

@inject(EDITOR_STORE)
@observer
class ControlPanel extends Component {
    constructor(props) {
        super(props);
        this.editorStore = props[EDITOR_STORE];
    }

    @computed
    get editorState() {
        return this.editorStore.state;
    }

    getSynonymClickHandler = (synonym) => {
        return () => this.editorStore.replaceBySynonym(synonym);
    };

    getFontStyleHandler = (style) => {
        return () => this.editorStore.setFontStyle(style);
    };

    render() {
        const {activeWord, style, synonyms, loading, synonymsLoadingError} = this.editorState;
        return (
            <div className="control-panel">
                <div className="format-actions">
                    <button
                        className={cx({
                            'format-action': true,
                            'active': activeWord && style[activeWord]?.bold,
                        })}
                        type="button"
                        onClick={this.getFontStyleHandler('bold')}
                    >
                        <b>B</b>
                    </button>

                    <button
                        className={cx({
                            'format-action': true,
                            'active': activeWord && style[activeWord]?.italic,
                        })}
                        type="button"
                        onClick={this.getFontStyleHandler('italic')}
                    >
                        <i>I</i>
                    </button>

                    <button
                        className={cx({
                            'format-action': true,
                            'active': activeWord && style[activeWord]?.underline,
                        })}
                        type="button"
                        onClick={this.getFontStyleHandler('underline')}
                    >
                        <span className="underline">U</span>
                    </button>
                </div>
                {!!synonyms.length ? (
                    <div className="synonyms-wrap">
                        <div className="synonyms">
                            <div className={cx({
                                'spinner': loading,
                            })}>
                                <div> </div> <div> </div>
                            </div>
                            <div className={cx({
                                'overlay': loading,
                            })}> </div>
                            <b>Synonyms</b>:
                            {synonyms.map((synonym, i) => (
                                <span key={synonym}>
                                {!!i && <span>,</span>}
                                    <span> </span>
                                <span
                                    className="synonym"
                                    onClick={this.getSynonymClickHandler(synonym)}
                                >
                                    {synonym}
                                </span>
                            </span>
                            ))}
                        </div>
                    </div>
                ) : !synonymsLoadingError ? (
                    <div className="synonyms">There are no synonyms for selected word...</div>
                ) : (<div className="synonyms">Can't get synonyms.</div>)}
            </div>
        );
    }
}

export default ControlPanel;
