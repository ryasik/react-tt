import React from 'react'
import './Footer.scoped.css';

const Footer = () => <div className={'footer'}>{new Date().getFullYear()}. Agile Engine</div>;

export default Footer
