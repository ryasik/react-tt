import React, {Component} from 'react';
import ControlPanel from './control-panel/ControlPanel';
import FileZone from './file-zone/FileZone';
import './App.scoped.css';
import Footer from "./footer/Footer";

class App extends Component {
    render() {
        return (
            <div className="app">
                <header className="header">
                    <span>Simple Text Editor</span>
                </header>

                <main className="main">
                    <ControlPanel />
                    <FileZone />
                </main>
                <Footer/>
            </div>
        );
    }
}

export default App;
