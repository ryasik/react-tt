import React, {Component} from 'react';
import {computed} from 'mobx';
import {inject, observer} from 'mobx-react';
import cx from 'classnames';
import {EDITOR_STORE} from '../../constants';
import './FileZone.scoped.css';

@inject(EDITOR_STORE)
@observer
class FileZone extends Component {
    constructor(props) {
        super(props);
        this.editorStore = props[EDITOR_STORE];
    }

    componentDidMount() {
        this.editorStore.setText();
    }

    @computed
    get editorState() {
        return this.editorStore.state;
    }

    getTextClickHandler = (word, i) => {
        return () => {
            if (word.match(/\w/)) {
                this.editorStore.setSynonyms(word);
                this.editorStore.setActiveWord(i);
            }
        };
    }

    render() {
        const {text, activeWord, style} = this.editorState;

        return (
            <div className="file-zone">
                <div className="file">
                    {text.map((word, i) => {
                        let classList = {};

                        if (style[i]) {
                            classList = style[i];
                        }
                        return (
                            <span
                                key={i}
                                className={cx({
                                    'word': word.match(/\w+/),
                                    'active-word': i === activeWord,
                                    'bold': classList.bold,
                                    'italic': classList.italic,
                                    'underline': classList.underline,
                                })}
                                onClick={this.getTextClickHandler(word, i)}
                            >
                                {word}
                            </span>
                        );
                    })}
                </div>
            </div>
        );
    }
}

export default FileZone;
